---
title: 【读源码】Vue中的$on和$emit的原理分析
date: 2021-02-24
sidebar: 'auto'
categories:
 - Vue
tags:
 - 读源码
 - 笔记
 - Vue
---
# 【读源码】Vue中的$on和$emit的原理分析

$on和$emit是Vue中常用的组合拳，前者用于给Vue实例绑定事件，后者用于触发事件。通过断点调试以及查看源码，得知它们具体的实现逻辑如下：

## $on()
1. $on方法接收两个参数，一个是事件名，一个是执行函数。
   
2. 通过this将Vue实例引入。首先判断传入的事件名是否为数组，如果不是数组，会在该Vue实例中的_events属性中注册该事件名并绑定执行函数，如果是数组则会遍历其中每一项事件名，并递归执行$on函数，逐个注册并绑定函数。(对数组的判断意味着$on可以实现对多个事件绑定同一执行函数，有助于减少开发中的代码量)
   
3. 最后返回该Vue实例。
   
   ```javascript
   Vue.prototype.$on = function (event, fn) {
      //引入Vue实例
      var vm = this;
      if (Array.isArray(event)) {
        for (var i = 0, l = event.length; i < l; i++) {
          vm.$on(event[i], fn);
        }
      } else {
        (vm._events[event] || (vm._events[event] = [])).push(fn);
        // optimize hook:event cost by using a boolean flag marked at registration
        // instead of a hash lookup
        if (hookRE.test(event)) {
          vm._hasHookEvent = true;
        }
      }
      return vm
    };
    ```
## $emit()
1. $emit接收一个参数，即字符串类型的event参数，其余的参数会作为触发事件所需要传递的参数。

2. $emit首先引入Vue实例，其次对event的命名进行检查，不符合规范的命名会被提示但不会中断$emit的执行。

3. 检查过后会在实例vm的`_event`中找出该事件(即先前通过$on绑定的事件)的回调函数，查找到的回调函数以数组cbs形式返回，于是会对其长度进行判断，刚长度大于1会遍历cbs，依次对其执行invokeWithErrorHandling()方法以执行回调函数或者改变作用域。

4. 最后返回该Vue实例。

```javascript
    Vue.prototype.$emit = function (event) {
      var vm = this;
      {
        var lowerCaseEvent = event.toLowerCase();
        if (lowerCaseEvent !== event && vm._events[lowerCaseEvent]) { 
          tip(
            "Event \"" + lowerCaseEvent + "\" is emitted in component " +
            (formatComponentName(vm)) + " but the handler is registered for \"" + event + "\". " +
            "Note that HTML attributes are case-insensitive and you cannot use " +
            "v-on to listen to camelCase events when using in-DOM templates. " +
            "You should probably use \"" + (hyphenate(event)) + "\" instead of \"" + event + "\"."
          );
        }
      }
      var cbs = vm._events[event];
      if (cbs) {
        cbs = cbs.length > 1 ? toArray(cbs) : cbs;
        var args = toArray(arguments, 1); //这一步将将回调函数所需的参数打包成一个数组，准备传递给invokeWithErrorHandling()
        var info = "event handler for \"" + event + "\"";
        for (var i = 0, l = cbs.length; i < l; i++) {
          invokeWithErrorHandling(cbs[i], vm, args, vm, info);
        }
      }
      return vm
    };
```

## invokeWithErrorHandling()
该函数主体为一个try...catch...结构，用于执行回调以及捕获错误信息

如果存在`args`参数，invokeWithErrorHandling()会通过apply执行回调函数，否则call一下改变作用域

```javascript
function invokeWithErrorHandling (
    handler, //回调函数
    context, //上下文
    args, //回调函数参数
    vm, //Vue实例
    info //错误信息
  ) {
    var res;
    try {
      res = args ? handler.apply(context, args) : handler.call(context);
      if (res && !res._isVue && isPromise(res) && !res._handled) {
        res.catch(function (e) { return handleError(e, vm, info + " (Promise/async)"); });
        // issue #9511
        // avoid catch triggering multiple times when nested calls
        res._handled = true;
      }
    } catch (e) {
      handleError(e, vm, info);
    }
    return res
  }
```

> 文章参考[通过Vue源代码解析$on、$emit实现原理](https://www.jianshu.com/p/02ab39b1cdac)