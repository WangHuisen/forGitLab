---
title: 【理解JS】再写一次Promise（二）
date: 2021-05-18
sidebar: 'auto'
categories:
 - Javascript
tags:
 - 异步编程
 - JS进阶
---

Promise本质上是一种状态管理机制，分为进行、解决、拒绝三种状态，用以表示表示异步任务的三种可能状态（正在进行、执行成功、执行失败）。

```javascript
// 定义三种状态
const PENDING = 'pending'
const RESOLVED = 'resolved'
const REJECTED = 'rejected'
```

除了状态本身，我们至少还需要4个参数，一是解决（resolved）后的值（称之为“值”可能不太容易理解，个人任务称之为消息更为贴切，用于Promise之间的通信），二是拒绝值，三是成功后的回调，四是失败后的回调。这里使用构造函数与原型的组合模式来构建Promsie（下篇会更详细的讲解对象生成方式）：

```javascript
function OwnPromise(executor) {
  // 初始化状态
  let self = this
  self.status = PENDING
  self.value = null
  self.error = null
  self.onFulfilledCallbacks = []
  self.onRejectedCallbacks = []
  // 定义resolve方法
  const resolve = (value) => {
    if(self.status !== PENDING) return // 排除重复解决的可能性
    setTimeout((value) => {
      self.status = RESOLVED
      self.value = value
      self.onFulfilledCallbacks.forEach(callback => callback(self.value)) // 逐个执行回调函数数组
    }, 0)
  }
  // 定义reject方法
  const reject = (value) => {
    if(self.status !== PENDING) return // 排除重复解决的可能性
    setTimeout((error) => {
      self.status = REJECTED
      self.error = error
      self.onRejectCallbacks.forEach(callback => callback(self.error))
    }, 0)
  }
  // 执行传入的函数
  try {
    executor(resolve, reject)
  } catch(e) {
    reject(e)
  }
}
OwnPromise.prototype.then = function(onFulfilled, onRejected) {
  if(this.status == PENDING) {
    this.onFulfilledCallbacks.push(onFulfilled)
    this.onRejectedCallbacks.push(onRejected)
  } else if (this.status == RESOLVED) {
    onFulfilled(this.value)
  } else {
    onRejected(this.error)
  }
  return this
}
```

上面的代码有几个点需要注意一下：

1. 解决或者拒绝的回调是一个数组，用于存储多个回调函数的情况。
2. PENDING到RESOLVED/REJECTED的状态是不可逆的

我们上一篇文章的例子来测试一下自己写的山寨版Promise：

```javascript
// 用Promise构造我们的异步任务
const asyncTask = function(name) {
  return new OwnPromise(function(resolve, reject) {
    try {
        resolve(name)
    } catch(e) {
        reject('出错了' + e)
      }
    })
}
// 定义失败的回调
const failCallback = (e) => {
  console.log(e)
}
// Promise的流畅写法
asyncTask('first')
.then(name => {
  console.log(name)
  return asyncTask('second')
})
.then((name) => {
  console.log(name)
  return asyncTask('third')
})
```

我们发现输出台只输出了:

```
first
first
```
为什么会这样？原因在于then返回的并不是我们创建的新的Promise，而是第一个Promise，导致了后续的then都共享第一个Promise的value。

```javascript
OwnPromise.prototype.then = function(onFulfilled, onRejected) {
  if(this.status == PENDING) {
    this.onFulfilledCallbacks.push(onFulfilled)
    this.onRejectedCallbacks.push(onRejected)
  } else if (this.status == RESOLVED) {
    onFulfilled(this.value)
  } else {
    onRejected(this.error)
  }
  return this // 关键在此！！！
}
```

此时this仍然指向第一个Promise，如果对于this指向问题存疑可以查看之前的文章。要解决这个问题，我们必须在then的返回结果上下功夫。

我们知道，合规的Promise支持链式调用，而就我们上面的情况而言，要实现链式调用的关键是将then回调的返回值用Promise包裹起来供后使用而非一直使用最初的那个Promise。我们改写一下then()：

```javascript
OwnPromise.prototype.then = function (onFulfilled, onRejected) {
  let self = this;
  if (self.status === PENDING) {
    return backPromise = new OwnPromise((resolve, reject) => {
      self.onFulfilledCallbacks.push((value) => {
        setTimeout(() ==> {
          try {
            let x = onFulfilled(value);
            resolve(x);
          } catch (e) {
            reject(e);
          }
        }, 0)
      });
      self.onRejectedCallbacks.push((error) => {
        setTimeout(() ==> {
          try {
            let x = onRejected(error);
            resolve(x);
          } catch (e) {
            reject(e);
          }
        }, 0)
      });
    })
  } else if (this.status === RESOLVED) {
    return backPromise = new OwnPromise((resolve, reject) => {
      setTimeout(() => {
        try {
          x = onFulfilled(self.value)
          resolve(x)
        } catch(e) {
          reject(e)
        }
      }, 0)
    })
  } else if (this.status === REJECTED) {
    return backPromise = new OwnPromise((resolve, reject) => {
      setTimeout(() => {
        try {
          x = onRejected(self.error)
          resolve(x)
        } catch(e) {
          reject(e)
        }
      }, 0)
    })
  } else {
    return this // 不传值默认返回上一个Promise
  }
```

上面的改写主要干了一件事，用Promise把then的回调函数返回值包裹，得到一个新的Promise。值得一提的是，使用setTimeout()是不得已为之，因为我们并没有权限直接构建微任务，只能用宏任务setTimeout来模拟。

但这里有些细节问题需要我们处理：

1. 假如then()不传回调函数或者传入其它类型的回调那么则无法继续链式调用。
2. 假如onFulfilled()/onRejected()返回的就是一个Promise，那么直接传给resolve()则存在问题了。

解决问题1，我们需要在then函数的最初对回调的类型进行一个判断

```javascript
// 判断两个处理参数是否为函数类型，如果不是则强行转换为函数类型
onFulfilled = typeof onFulfilled === "function" ? onFulfilled : value => value
onRejected = typeof onRejected === "function" ? onRejected : error => { throw error }
```

解决问题2，我们可以把回调返回的Promise再次解决一遍，直到拿到最终的值。

```javascript
// 单独定义解析返回Promise的方法
function resolvePromise(backPromise, x, resolve, reject) {
  // 排除重复引用的情况
  if(backPromise === x) {
    return reject(new TypeError('重复引用相同的Promise!'));
  }
  if (x instanceof OwnPromise) {
    if (x.status === PENDING) {
      x.then(y => {
        resolvePromise(backPromise, y, resolve, reject)
      }, error => {
        reject(error)
      })
    } else {
      x.then(resolve, reject)
    }
  } else {
    resolve(x)
  }
}
```

我们再把then()中的resolve改写为resolvePromise()就能够实现最终的效果啦：

```javascript
// 实例测试
const asyncTask = function(name) {
  return new OwnPromise(function(resolve, reject) {
    try {
      resolve(name)
    } catch(e) {
      reject('出错了' + e)
    }
  })
}

asyncTask('first')
.then(name => {
  console.log(name)
  return asyncTask('second')
})
.then((name) => {
  console.log(name)
  return asyncTask('third')
})
.then(name => {
  console.log(name)
  return asyncTask('forth')
})
```

成功输出:

```
first
second
third
```

有了前面的铺垫，catch()也很好实现了：

```javascript
OwnPromise.prototype.catch = function(onRejected) {
  return this.then(null, onRejected)
}
```

显而易见，catch是then(null, onRejected)的低级语法糖，很容易理解。

到这里，我们已经完成了Promise的主要功能了，下一篇讲一讲Promise的主要api的实现😁。

（[点击下载本章完整代码](https://github.com/WangHuisen/blogCode/blob/master/promise.js)）

