---
title: 【理解JS】再写一次Promise（一）
date: 2021-05-17
sidebar: 'auto'
categories:
 - Javascript
tags:
 - 异步编程
 - JS进阶
---

接着上篇讲，我提到Promise属于微任务，很好的解决了JS语言内部的异步问题。对比以往的嵌套回调的异步方式，Promise的写法更加人性，符合人类大脑线性思维的特点。

来看一个例子：

```javascript
// 定义一个异步任务
const asyncTask = function(name, success, fail) {
  try {
    setTimeout(function() {
      console.log(name)
      success()
    }, 0)
  } catch(e) {
      throw Error('出错了！' + e)
      fail()
    }
  }
// 按回调方式执行
const failCallback = () => {
  console.log('failed')
}
asyncTask('first', function() {
  asyncTask('second', function() {
    asyncTask('third', function() {
      console.log('ok')
    }, failCallback)
  }, failCallback)
}, failCallback)
```
有没有发觉这样子的写法非常让人难受，有没有觉得在理解其含义时心中堵的慌？层层嵌套使得代码的可读性大大降低，带来了大量的冗余代码（失败回调每次都要声明一次），这就是常常被人诟病的“回调地狱”，Promise的出现拯救了这种状况：
```javascript
// 用Promise构造我们的异步任务
const asyncTask = function(name) {
  return new Promise(function(resolve, reject) {
    try {
      setTimeout(function() {
        console.log(name)
        resolve()
      }, 0)
    } catch(e) {
        reject('出错了' + e)
      }
    })
}
// 定义失败的回调
const failCallback = (e) => {
  console.log(e)
}
// Promise的流畅写法
asyncTask('first')
.then(() => {
  return asyncTask('second')
})
.then(() => {
  return asyncTask('third')
})
.catch(failCallback())
```
这种写法至少从三个方面改进了异步编写的体验：

1. 回调函数从主函数分离，阅读更加清晰
2. 线性调用回调函数而非嵌套，编写更加符合思维逻辑
3. 失败回调只在最后声明一次，由Promise内部的错误冒泡机制实现

下篇讲Promise是如何实现上述功能的（之前写过一篇用ES6语法的class版本，这次会更加详尽）。