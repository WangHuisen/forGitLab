---
title: 【理解JS】this指向问题
date: 2021-04-17
sidebar: 'auto'
categories:
 - Javascript
tags:
 - JS进阶
---

this的取值问题在Javascript开发中是常见的，因其动态性常常让人琢磨不透，本文总结了几种常见的this指向问题。

在了解this指向之前，我们有必要理解变量与作用域的特性。

JS中，自由变量的查找是静态的：
```javascript
// 自由变量的查找是基于函数定义的地方，是静态的
function hello () {
  const a = '111'
  return function () {
    console.log(a)
  }
}
const res = hello()
const a = '222'
res() // 返回函数定义在hello函数内部，闭包保留了hello内部定义的变量，a可以正常查找因此输出111
```
而this和作用域相关，更准确来说，和正在执行的作用域相关：
```javascript
// 而this由其调用者（或者说调用位置）决定，是动态的
function hello () {
  return function () {
    console.log(this)
  }
}
hello()() // 输出window对象
```
以上是this指向的典型情况，下面罗列了其它常见情况：

第一，this指向函数的调用者：
```javascript
const test1 = {
  a: 'inner',
  func: function () {
    return this.a
  }
}
console.log(test1.func()) // 调用者为test1对象，其作用域内定义了变量a, 因此正常输出inner
```
第二，this指向全局上下文：
```javascript
var a = 'outer'
const test2 = {
  a: 'inner',
  func: function () {
    return this.a
  }
}
const f = test2.func
console.log(f()) // 在全局上下文调用，输出outer
console.log(this) // 浏览器环境输出最顶层的window对象，node环境输出{}
```
第三，this指向构造函数所创建的对象：
```javascript
function Func() {
  this.a = 'a'
}
let f2 = new Func()
console.log(f2.a)
f2.a = 'b' // 对f2.a进行重新赋值
console.log(f2.a) // 显而易见，this绑定在新构造的函数上,输出b
```
第四，箭头函数，箭头函数很特殊，没有自己的this，默认使用其所在作用域层级上最近的函数作用域。
```javascript
var a = 'outer';
const test3 = {
  a: 'inner',
  func: () => {
    console.log(this.a)
  } // 箭头函数的this位于距离其层级最近的函数作用域（而非块级作用域）
}
test3.func(); // 输出outer，对比第一种情况更容易理解
```
为了更好的理解箭头函数的this机制，这里有必要拓展一下，援引[MDN](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/arrow_functions)的例子；
```javascript
var adder = {
  base : 1,

  add : function(a) {
    var f = v => v + this.base;
    return f(a);
  },

  addThruCall: function(a) {
    var f = v => v + this.base;
    var b = {
      base : 2
    };

    return f.call(b, a);
  }
};

console.log(adder.add(1));         // 输出 2
console.log(adder.addThruCall(1)); // 仍然输出 2
```
为什么说箭头函数的this是其最近的函数作用域？在这个例子显而易见，如果把上面addThruCall函数内的f改为用function声明的函数，情况则如你所愿，call改变作用域进而输出3。

第五，DOM事件绑定。这种情况显而易见，不举例讲了，this指向被绑定的DOM元素。

总结一句话，this动态的由函数调用者决定，除了自身无this的箭头函数（箭头函数的this指向最近的用function声明的函数作用域）。

（最近博客会写的更频繁，下篇讲浏览器的事件循环机制。）