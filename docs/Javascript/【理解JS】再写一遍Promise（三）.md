---
title: 【理解JS】再写一遍Promise（三）
date: 2021-05-21
sidebar: 'auto'
categories:
 - Javascript
tags:
 - 异步编程
 - JS进阶
---

这章内容就相对简单了，主要基于我们上一节已经实现的OwnPromise。主要实现了Promise.resolve()、Promise.reject()、Promise.finally()、Promise.all()、Promise.race()，大多使用静态方法构建。

### Promise.resolve()

该方法主要解决传入对象不是Promise的情况而是将传入值进行包装，返回一个新的Promise。

```javascript
// Promise的静态方法resolve()
OwnPromise.resolve = res => {
  // 本身是Promise直接返回
  if (res instanceof OwnPromise) return res
  return new OwnPromise((resolve, reject) => {
    // 针对thenable接口的处理（即，非Promise但具有then方法的对象）
    if (res && res.then && typeof res.then === 'function') {
      res.then(resolve, reject)
    } else {
      // 其他值直接解决
      resolve(res)
    }
  })
}
```

### Promise.reject()

reject方法很简单，直接对值进行拒绝并返回新的Promsie。

```javascript
// Promise的静态方法reject()
OwnPromise.reject = error => {
  return new OwnPromise(
    reject(error)
  )
}
// 可以对传入值进行判断以防非法类型，这里不细说，类似Promise.resolve()
```

### Promise.finally()

Promise.finally()不管前面Pomise是解决还是失败，都会执行传入其中的回调，按原来的顺序将值往下传递。

```javascript
// Promise的静态方法finally()
OwnPromise.prototype.finally = callback => {
  this.then(value => {
    return OwnPromise.resolve(callback()).then(() => {
      return value
    })
  }, error => {
    return Promise.resolve(callback()).then(() => {
      throw error
    })
  })
}
```

需要注意的是，按照官方的做法，我把finally定义在了Promise的原型里，减少内存开销。

### Promise.all()

all()方法接收一个数组，并返回一个对其中每个元素进行Promise解决后的数组。整体思路并不复杂：

```javascript
// Promise.all()
OwnPromise.all = promiseArray => {
  return new OwnPromise((resolve, reject) => {
    // 必须返回一个数组
    let resArray = [],
    i = 0
    // 对传入参数进行类型判断
    if (!Array.isArray(promiseArray)) {
      return reject(new TypeError('Promise.all()必须传入一个数组！'))
    }
    // 对传入数组为空的情况进行处理
    if(promiseArray.length === 0) {
      return resolve(resArray)
    }
    // 将单个元素的解决值注册到返回数组中
    function handleEach(index, data) {
      resArray[index] = data
      i++
      if (i === promiseArray.length) { // 注册完毕后将返回数组解决
        resolve(resArray)
      }
    }
    // 遍历数组，取出解决值
    for (let k = 0; k < promiseArray.length; k++) {
      // 为了处理不是Promise类型的情况，需要用Promsie.resolve()新建一个Promise
      OwnPromise.resolve(promise[i]).then(data => {
        handleEach(k, data)
      }).catch(err => {
        reject(err)
      })
    }
  })
}
```

### Promise.race()

Promise.race()与Promise.all()想反，只要一个数组中有一个Promise得到解决或拒绝就直接

```javascript
// Promise.race()
OwnPromise.race = promiseArray => {
  return new OwnPromise((resolve, reject) => {
    if (!Array.isArray(promiseArray)) {
      return reject(new TypeError('Promise.all()必须传入一个数组！'))
    }
    // 对传入数组为空的情况进行处理
    if(promiseArray.length === 0) {
      return resolve([])
    }
    for (let i = 0; i < promiseArray.length; i++) {
      OwnPromise.resolve(promiseArray[i]).then(data => {
        resolve(data)
        return
      }).catch(err => {
        reject(err)
        return
      })
    }
  })
}
```

不得不承认，我对上述API的用法还不甚理解，后面会具体了解其中用法及其设计用意。

到这里，手写Promise的任务基本上已经完成，下篇将将别的异步方案。