---
title: 【理解JS】闭包是啥？
date: 2021-04-1
sidebar: 'auto'
categories:
 - Javascript
tags:
 - JS进阶
---
我们知道，在JavaScript中，存在着一条单向的作用域链。称之为单向，意味着子级作用域可以访问父级作用域，而父级作用域无法访问子级作用域。
```javascript
var a = 'outer';
function foo() {
    var b = 'inner'
    console.log(a);
}
foo(); // 'outer'
console.log(b) // 报错，b未定义
```
闭包正是解决外部作用域无法访问内部作用域的方案，在JavaScript中，我们把包含内部变量的函数作为返回值可以达到不让浏览器内存清理机制清除内部变量的目的。这是最典型的闭包方案。
```javascript
function foo() {
    var a = 'inner';

    function closure() {
        console.log(a)
    };

    return closure
}
var res = foo();
res() // 'innner'
```
可以在结果中看到，在全局作用域下，我们也成功得到了foo()函数内部的变量，你可能觉得这没什么，直接`return a`岂不是更加简洁方便，但请注意我们得到的是变量a而不是a的值'inner'，看下面的例子就明白了:
```javascript
// 函数内的变量会因为闭包的存在而一直保留在内存之中
function foo() {
    var count = 0;

    function add() {
        count++;
        console.log(count)
    };

    return add
}
var res = foo();
res(); // 1
res(); // 2
```
可以看到，变量count连续两次执行了自增操作，如果直接调用闭包函数则会导致另一种结果，即闭包在执行完后立即执行垃圾清理，没有我们想要的效果:
```javascript
//一次性调用闭包会销毁，内存不会保存函数内的变量
function foo() {
    var count = 0;

    function add() {
        count++;
        console.log(count)
    };

    return add
}
foo()(); // 1
foo()(); // 1
```
还有一种闭包的形式是把函数作为参数传递:
```javascript
// 2. 函数作为参数传递
var a = 'outer';
function foo(a) {
    console.log(a);
}
function bar() {
    var a = 'inner';
    foo(a);
}
bar(); // 'inner'
```
我们在bar()函数外另外定义了一个foo()函数，并在bar()函数内调用foo()，发现输出结果不是'outer'而是'innner'，foo()函数成功引用了bar内部的的变量。但是，foo()函数实际执行的作用域仍热是其定义之处——全局作用域:
```javascript
// 但注意，作为参数的函数其调用位置仍然是外部
var a = 'outer';
function foo() {
    console.log(a);
}
function bar() {
    var a = 'inner';
    foo();
}
bar(); // 'outer'
```