---
title: 【理解JS】用class实现一个Promise
date: 2021-03-09
sidebar: 'auto'
categories:
 - Javascript
tags:
 - 异步编程
 - JS进阶
---

过去的js开发者常常苦于因JavaScript单线程的特性在执行大量异步操作时产生的“回调地狱”问题，民间开发者创造了Promise(主流中文译名为“期约”或者“承诺”)来管理异步操作的执行结果，后来在ECMAScript2015中明确将Promise规范化，详情可以查看[Promise/A+](https://promisesaplus.com/)的原文。本文我记录了我用class实现的一个符合规范的Promise类。

```javascript
      // 1. 定义三种promise状态
      const PENDING = 'pending';
      const RESOLVED = 'resolved';
      const REJECTED = 'rejected';
      // 2. 构建类
      class Promise {
        constructor(executor) { // 传入的是一个执行函数
          this.status = PENDING;
          this.success = ''; // 保存resolved时传入的数据
          this.reason = ''; // 保存rejected时传入的数据
          //之所以定义一个容器数组，是为了存放pending时的回调函数
          this.onFulfilledCallbacks = [] // 保存成功回调函数的容器
          this.onRejectedCallbacks = [] // 保存失败回调函数的容器
          
          // 因为每个promise实例必须具有改变状态的能力，所以resolve和reject定义在constructor()中
          let resolve = (success) => {
            if(success instanceof Promise) { // 处理success值也是一个promise的情况
              success.then(resolve, reject); // 使用当前promise实例的resolve()或者reject()递归解析
            }
            if(this.status === PENDING) {
              this.success = success;
              this.status = RESOLVED;
              this.onFulfilledCallbacks.forEach(fn => fn());
            }
          };
          let reject = (reason) => {
            if(this.status === PENDING) {
              this.reason = reason;
              this.status = REJECTED;
              this.onRejectedCallbacks.forEach(fn => fn());
            }
          };
          // 执行传入的执行函数
          try {
            executor(resolve, reject);
          } catch(e) {
            console.log('catch到了错误:', e);
            reject(e); // 手动调用reject()函数
          }
        };
        // 定义thenable接口,绑定在Promise的原型上
        then(onFulfilled, onRejected) { // 参数为两个函数：一个处理resolved的函数，一个处理rejected的函数
          // 判断两个处理参数是否为函数类型，如果不是则强行转换为函数类型
          onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : val => val;
          onRejected = typeof onRejected === 'function' ? onRejected : err => {throw err};
          let backPromise = new Promise((resolve, reject) => {
            // 用来处理异步的情况(异步时传递的status就是初始化时的pending状态)
            if(this.status === PENDING) {
              this.onFulfilledCallbacks.push(() => {
                setTimeout(() => { // 使用定时器异步执行即可取到backPromise
                  try {
                    let x = onFulfilled(this.success);
                    resolvePromise(backPromise, x, resolve, reject);
                  } catch(e) {
                    reject(e);
                  }
                }, 0)
              });
              this.onRejectedCallbacks.push(() => {
                setTimeout(() => {
                  try {
                    let x = onRejected(this.reason);
                    resolvePromise(backPromise, x, resolve, reject);
                  } catch(e) {
                    reject(e);
                  }
                }, 0);
              });
            }
            // 处理正常的同步情况
            if(this.status === RESOLVED) {
              setTimeout(() => { // setTimeout是宏任务，把回调函数放入定时器后续执行即可取到backPromise
                try {
                  let x = onFulfilled(this.success);
                  resolvePromise(backPromise, x, resolve, reject);
                } catch(e) {
                  reject(e);
                }
              }, 0)
            }
            if(this.status === REJECTED) {
              setTimeout(() => {
                try {
                  let x = onRejected(this.reason);
                  resolvePromise(backPromise, x, resolve, reject);
                } catch(e) {
                  reject(e);
                }
              }, 0)
            }
          });
          return backPromise;
        }
      }
      // 3. 单独抽离resolvePromise()函数，用来将处理值解决并转为一个promise实例
      function resolvePromise(backPromise, x, resolve, reject) {
        // 判断then()函数中返回值x的类型是关键
        if(backPromise === x) {
          return reject(new TypeError('重复引用相同的Promise!'));
        }
        if((typeof x === 'object' && x !== null) || typeof x === 'function') { // x是非null对象或者函数的情况
          let then = x.then // 如果是promise对象，必有then方法
          if(typeof then === 'function') { // 如果then为函数则认定x为一个promise实例
            then.call(x, res => { // 在x的作用域内执行then
              resolvePromise(backPromise, res, resolve, reject); // promise解析出来仍然可能是一个promise，故递归
            }, err => {
              reject(err); // 报错则reject
            })
          } else { // x为普通对象但存在then属性或者方法,比如{name: 'whs', then() {console.log('Hi!')}}
            resolve(x); // 直接解决该对象
          }
        } else { // 非函数或者对象，x为一般字符串或者数值时直接解决
          resolve(x);
        }
      }

```