---
title: 【浏览器】理解浏览器的事件循环（EventLoop）机制
date: 2021-05-07
sidebar: 'auto'
categories:
 - 浏览器
tags:
 - 异步编程
 - 浏览器
---

浏览器的事件循环机制主要为了解决JS的异步问题。

众所周知，Javascript是单线程的，理论上来说，仅仅依赖JS我们只能得到可能阻塞的同步代码。但在浏览器中，大量的GUI渲染、Ajax、DOM事件、浏览器插件等并没有阻塞网页的正常显示，这就意味着这些任务不是由JS负责的，浏览器存在着其它线程/进程与JS同时在执行对应任务。

事实上，浏览器本身是多进程的，主要包含：

1. 浏览器进程
2. 网页渲染进程
3. GPU进程
4. 网络进程
5. 浏览器插件进程
···

而我们主要操作的是和HTML、CSS、JS相关的渲染进程。渲染进程下又包含了如下数个线程：

1. GUI线程（主要负责解析HTML并生成DOM树和CSS渲染）
2. JS引擎线程（主要是JS的解释与执行）
3. 定时器触发线程（我们在JS中调用setTimeout和setInterval时就会指派给该线程进行定时操作）
4. 事件触发线程（主要负责监听DOM事件）
5. XHR线程（负责发起HTTP请求）
...

而JS可以视作能够调用其他线程的一条“主线程”, 在等待其它线程结果的过程中产生了等待，进而引发了JS的异步问题（但本质上这种异步是同步执行的，只能算一种对异步的模拟）。

如何保证JS在其它线程任务执行时不被阻塞，浏览器提供了事件循环机制（EventLoop）来保证等待异步任务和执行同步任务执行的顺序。

来看一段简单的代码：

```javascript
console.log(1)
setTimeout(() => {
  console.log(2)
}, 1000)
console.log(3)
```

如果按照同步的思维去理解这段代码，输出是：1，2，3，但事实上却输出：1，3，2。显然，setTimeout的回调被放在了最后执行。

为什么会这样？前面说到定时器是由一个专门的定时器线程负责定时操作。在解释为什么回调被放在最后之前不妨反向思考一下，假如回调函数是按照顺序执行会怎样？

显然这会导致阻塞，上面的代码很简单并不会有什么实质性问题，但假如setTimeout后是成百上千行代码则带来的阻塞是无法忍受的。

这就是事件循环要解决的问题。

事件循环（Event Loop）是一种程序结构，用于等待和发送信息的事件。它由JS的运行环境提供，比如浏览器、Node等待。这种结构的思路可以简单概况为：

同步任务先执行，异步任务入任务列队，等待同步任务执行完后依次执行。

这句话已经能够帮助我们解决绝大部分异步问题了。

```javascript
console.log('start')
new Promise(resolve => {
  console.log('Promise init')
  resolve()
})
.then(() => {
  console.log('Promise then')
})
console.log('step')
```

输出：

```
start
Promise init
step
Promise then
```

我们把两个例子合并一下:

```javascript
console.log('start')
setTimeout(() => {
  console.log('setTimeout callback')
}, 1000)
new Promise(resolve => {
  console.log('Promise init')
  resolve()
})
.then(() => {
  console.log('Promise then')
})
console.log('step')
```

你可能以为会输出：

```
start
Promise init
step
setTimeout callback
Promise then
```

但实际上：

```
start
Promise init
step
Promise then
setTimeout callback
```

setTimeout的回调被放到了Promise.then()的后面执行，即使它定义在Promise.then()之前。

这里就要引申出宏任务与微任务的概念了，事件循环机制存在着两种不同的任务列队以理清异步任务的优先级。

我把宏任务理解为其它浏览器线程的异步任务，而微任务是JS语言内部的异步任务，它被被安排在某个宏任务后执行。

而宏任务与微任务的执行顺序是：先执行宏任务列队的第一个宏任务，如果该次任务存在微任务，则在第一条宏任务后建立微任务列队，第一条宏任务执行完毕后按次序执行微任务列队。微任务列队执行完毕后执行第二条宏任务，以此类推。借用一张经典的流程图：

<img :src="$withBase('/imgs/浏览器/事件循环.png')" alt="事件循环" width="50%" height="50%">

由此我们可以理解更为复杂的异步任务背后的执行顺序了：

```javascript
console.log('Loop Start')
new Promise(resolve => {
  console.log('Promise Start1')
  resolve()
})
.then(() => {
  console.log('Promise then1')
})
setTimeout(() => {
  console.log('setTimeout1')
  new Promise(resolve => {
    console.log('Promise Start2')
    resolve()
  })
  .then(() => {
    setTimeout(() => {
      console.log('setTimeout2')
    }, 0)
    console.log('Promise then2')
  })
}, 0)
console.log('step')
```

输出：

```
Loop Start (宏1)
Promise Start1 (宏1)
step (宏1)
Promise then1 (微1)
setTimeout1 (宏2)
Promise Start2 (宏2)
Promise then2 (微2)
setTimeout2 (宏3)
```

到这里，你应该大致理解事件循环机制了吧。顺着异步这一话题，下篇讲Promise。