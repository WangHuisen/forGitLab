---
title: 【小脚本】爬取微博评论数据
date: 2021-04-19
sidebar: 'auto'
categories:
 - Python
tags:
 - 实用脚本
---

最近做一个期中作业，需要用到微博评论的数据，故今天花了一晚上时间赶出了简单的小脚本，主要基于requests+pyquery，后续会跟进一个基于asyncio+aiohttp的异步实现的版本。

下面是完整代码：
```python
# -*- coding: utf-8 -*-
#author: 王汇森
#date: 2021.4.19
#address: Soochow University
#Github: https://github.com/WangHuisen

import requests
from pyquery import PyQuery as pq
import re
import csv
import time

# 浏览器请求头 cookie以自己的为准
headers = {
  'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36',
  'cookie': 'SINAGLOBAL=373010613050.5928.1593854731501; SUB=_2A25NfhcjDeThGeNG6loU9S3FzD-IHXVugLlrrDV8PUJbkNANLWH8kW1NS2wCgJSc5Dq3OnzNSNuNhz4QLDFlSZVd; SUBP=0033WrSXqPxfM725Ws9jqgMF55529P9D9Wh4rj2cOYxeOjRsKK3VOeAI5NHD95Qf1h2RSK-01KM0Ws4DqcjSBg4adrL_dN-fe0Mp1Btt; wvr=6; wb_view_log_5818553973=1536*8641.25; UOR=,,cn.bing.com; _s_tentry=s.weibo.com; Apache=1833020534959.3643.1618810202723; ULV=1618810202776:12:4:2:1833020534959.3643.1618810202723:1618801519836; webim_unReadCount=%7B%22time%22%3A1618814046415%2C%22dm_pub_total%22%3A0%2C%22chat_group_client%22%3A0%2C%22chat_group_notice%22%3A0%2C%22allcountNum%22%3A2%2C%22msgbox%22%3A0%7D'
}
# 评论根页面
baseUrl = 'https://weibo.cn/comment/{}'
# 评论页
commentUrl = 'https://weibo.cn/comment/{}?page={}'
# csv文件行首
header = ['序号', '用户ID', '评论内容', '发布时间']
# 博文池 记载各博文ID
blogPools = ['KaJM85Mkz', 'KaKuvd7sG', 'KaJKJfqqQ', 'KaJQ0oaKi', 'KaJOea40I']
# 计数器
count = 1
# csv writer
writer = None



# 获取总的分享数、评论数、赞数
def getCommentsNum(baseUrl=baseUrl):
  res = requests.get(baseUrl.format('KbrAclBKW'), headers=headers)
  doc = pq(res.text.encode('utf-8'))

  allShares = doc('body > div:nth-child(7) > span:nth-child(1) > a').text()
  allShares = allShares[3:len(allShares)-1]
  
  allComments = doc('span.pms').text()
  allComments = allComments[3:len(allComments)-1]
  
  allLikes = doc('body > div:nth-child(7) > span:nth-child(3) > a').text()
  allLikes = allLikes[2:len(allLikes)-1]

  allPages = doc('#pagelist > form > div').text()
  allPages = re.search(r"/(\d+)页", allPages).group(1)
  # print(allPages.group(1))
  # print([allShares, allComments, allLikes])
  return [allShares, allComments, allLikes, allPages]

# 解析评论页
def parseCommentPage(url):
  res = requests.get(url, headers=headers)
  doc = pq(res.text.encode('utf-8'))
  items = doc('div[id^="C"]').items()
  return items

# 获得评论数据
def getCommentsData(item):
  userID = item('div[id^="C"]>a:nth-of-type(1)').text()
  content = item('span.ctt').text()
  time = item('span.ct').text()
  time = re.search('(.*)来自网页', time).group(1)[1:]
  return [userID, content, time]

# 写入csv
def writeToCsv(writer, row):
    writer.writerows(row)

# 爬取一条博文的评论
def scrapyOne(key):
  global baseUrl, commentUrl, header, count
  baseUrl = baseUrl.format(key)
  allShares, allComments, allLikes, allPages = getCommentsNum(baseUrl)
  print('该博文共有' + allComments + '条评论 ' + allShares + '次分享 ' + allLikes + '个赞👍')

  for i in range(1, int(allPages)+1):
    nowUrl = commentUrl.format(key, i)
    print('正在抓取{}'.format(nowUrl))
    items = parseCommentPage(nowUrl)
    res = []
    for item in items:
      row = getCommentsData(item)
      row.insert(0, count)
      print(row)
      res.append(row)
      count += 1
    writeToCsv(writer, res)
    
# 主函数
def main():
  global writer
  print(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
  f = open('./weiboComments.csv', 'a', encoding='utf-8-sig', newline='')
  writer = csv.writer(f)
  writer.writerows([header])
  for i in blogPools:
    scrapyOne(i)
  f.close()
  print(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
  

if __name__ == '__main__':
  main()
```