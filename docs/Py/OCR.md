---
title: 一个利用百度API实现文字识别的小脚本
date: 2021-02-24
sidebar: 'auto'
categories:
 - Python
tags:
 - 实用脚本
---

# 一个利用百度API实现文字识别的小脚本

昨天为了帮助一个同学翻译一篇英文文献，写了一个小脚本，完整代码贴在下面。

你只需要填好需要识别的图片的路径，再在百度智云网站上申请一个有效的API密钥传入其中即可轻松导出需要识别文字的txt格式文件。

该脚本主要参考百度官方提供的实例脚本，但使用requests库重写并改进了一遍。

```python
import requests
import base64
import json
from urllib.parse import urlencode
from urllib.error import URLError
import os

#这些参数都可以在百度智云API管理界面查看到
API_KEY = 'wQO7a7ldtzccRnZ******' 
SECRET_KEY = 'McKNZAxeM4AqKzaond1l********'
OCR_URL = "https://aip.baidubce.com/rest/2.0/ocr/v1/accurate_basic"
TOKEN_URL = 'https://aip.baidubce.com/oauth/2.0/token'

#这一步主要目的是获得必要的token
def fetch_token():
    params = {'grant_type': 'client_credentials',
            'client_id': API_KEY,
            'client_secret': SECRET_KEY}
    post_data = urlencode(params)
    res = requests.get(TOKEN_URL, params=post_data, timeout=5)
    f = res.content
    result = json.loads(f.decode())
    # print(result)

    if ('access_token' in result.keys() and 'scope' in result.keys()):
        if not 'brain_all_scope' in result['scope'].split(' '):
            print ('please ensure has check the  ability')
            exit()
        return result['access_token']

#读取图片
def read_file(image_path):
    f = None
    try:
        f = open(image_path, 'rb')
        return f.read()
    except:
        print('read image file fail')
        return None
    finally:
        if f:
            f.close()

#发送post请求，获得识别后的文字
def get_data(url, data):
    try:
      f = requests.post(url, data=data.encode('utf-8'))
      res = f.text
      return res
    except URLError as err:
      print(err)

#主函数，获得文本并存储为一个txt文件
def main():
    token = fetch_token()
    image_url = OCR_URL + "?access_token=" + token
    with open('res.txt', 'w') as f:
      files = os.listdir('./imgs')
      for file in files:
        text = ''
        file = read_file('./imgs/' + file)
        result = get_data(image_url, urlencode({'image': base64.b64encode(file)}))
        result_json = json.loads(result)
        for words_result in result_json["words_result"]:
            text = text + words_result["words"]
        f.write(text)

if __name__ == '__main__':
    main()
```